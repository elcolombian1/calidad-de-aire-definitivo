import time 
import Adafruit_DHT 
import datetime 
import MySQLdb
from PMS7003 import PMS7003	 as PMSENSOR


class save_data(): ## Save Data
	def __init__(self):
			try:
				self.db =MySQLdb.connect(host="localhost", user="rootpi", passwd="root",db="datos")
				self.cursor=self.db.cursor()	
				print("Conexion Exitosa")
				self.db.commit()
		
			except Exception as e:
				print(str(e)+"Conexion a base de datos fallida")

	def save_temperature(self,value):
		try:
			self.cursor.execute("INSERT INTO sensor_reading_dato (tipo_dato,valor_dato,fecha) VALUES ('Temperatura',%s,%s)",(value,datetime.datetime.now()))
		except Exception as e:
			self.db.commit()
			print("Error al guardar"+" "+str(value)+" "+str(e))

		

	def save_humidity(self,value):	
		try:
			self.cursor.execute("INSERT INTO sensor_reading_dato (tipo_dato,valor_dato,fecha) VALUES ('Humedad',%s,%s)",(value,datetime.datetime.now()))
		except Exception as e:
			self.db.commit()
			print("Error al guardar"+" "+str(value)+" "+str(e))

		
	
	def save_CO2(self,value):
		try:
			self.cursor.execute("INSERT INTO sensor_reading_dato (tipo_dato,valor_dato,fecha) VALUES ('CO2',%s,%s)",(value,datetime.datetime.now()))
			self.db.commit()
		except Exception as e:
			print("Error al guardar"+" "+str(value)+" "+str(e))

		
	def save_O3(self,value):
		try:
			self.cursor.execute("INSERT INTO sensor_reading_dato (tipo_dato,valor_dato,fecha) VALUES ('O3',%s,%s)",(value,datetime.datetime.now()))
			self.db.commit()
		except Exception as e:
			print("Error al guardar"+" "+str(value)+" "+str(e))

		
	def save_CO(self,value):
		try:
			self.cursor.execute("INSERT INTO sensor_reading_dato (tipo_dato,valor_dato,fecha) VALUES ('CO',%s,%s)",(value,datetime.datetime.now()))
			self.db.commit()
		except Exception as e:
			print("Error al guardar"+" "+str(value)+" "+str(e))

			
	def save_NH3(self,value):
		try:
			self.cursor.execute("INSERT INTO sensor_reading_dato (tipo_dato,valor_dato,fecha) VALUES ('NH3',%s,%s)",(value,datetime.datetime.now()))
			self.db.commit()
		except Exception as e:
			print("Error al guardar"+" "+str(value)+" "+str(e))

			
	def save_NO2(self,value):
		try:
			self.cursor.execute("INSERT INTO sensor_reading_dato (tipo_dato,valor_dato,fecha) VALUES ('NO2',%s,%s)",(value,datetime.datetime.now()))
			self.db.commit()
		except Exception as e:
			print("Error al guardar"+" "+str(value)+" "+str(e))

				
	def save_PM2(self,value):
		try:
			self.cursor.execute("INSERT INTO sensor_reading_dato (tipo_dato,valor_dato,fecha) VALUES ('PM2',%s,%s)",(value,datetime.datetime.now()))
			self.db.commit()
		except Exception as e:
			print("Error al guardar"+" "+str(value)+" "+str(e))


	def save_PM10(self,value):
		try:
			self.cursor.execute("INSERT INTO sensor_reading_dato (tipo_dato,valor_dato,fecha) VALUES ('PM10',%s,%s)",(value,datetime.datetime.now()))
			self.db.commit()
		except Exception as e:
			print("Error al guardar"+" "+str(value)+" "+str(e))


	def close_db(self):
		try:
			self.cursor.close()
		except Exception as e:
			print("Error al guardar"+" "+str(value)+" "+str(e))


class sensor_reader():
        def __init__(self):
		self.kind_sensor = Adafruit_DHT.DHT11 # Tipo de sensor de humedad usado
		self.temp_hum_pin = 18 ##Pin para recepcion de informacion desde pines del sensor de humedad

	def get_data_temp_humidity(self):##implemenatacion de metodo para obtener informacionde sensor de temperatura
				
		res=Adafruit_DHT.read_retry(self.kind_sensor,self.temp_hum_pin)
                return(res)

	def get_pm_data(self):
    		pm2_10=PMSENSOR()
	    	buffer_aux=pm2_10.cargar_buffer()
	    	pm2,pm10=pm2_10.return_data(buffer_aux)
	    	return(pm2,pm10)

