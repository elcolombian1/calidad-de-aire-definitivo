from classes import sensor_reader,save_data
import time			
from mq import *
import sys, time
import random
def main():
	
		# mq_CO2 = MQ(1,0,-2.802,127.8,0.7)
		# mq_CO = MQ(1,0,-4.771,1254.4,0.1)
		# mq_O3 = MQ(0,0,-1.0432297135,42.85561841,0.2)
		mq_CO2 = MQ(1,0,-0.802,327.8,130);
		mq_CO = MQ(1,0,-1.771,1254.4,1);
		mq_O3 = MQ(0,0,-1.0432297135,42.85561841,0.007);
		mq_NH3 = MQ(2,0,-1.73,0.7382,0.7)
		mq_NO2= MQ()
		
		##Llama a la clase MQ para lectura de GAS con curva para CO2  y el pin del ADC y offset
		
		data_manage=save_data()#Inicializa objeto de la clase base de datos para alojar datos 
		read_sensor=sensor_reader()#inicializa objeto para lectura de datos
		
		while(True):
			try:	

				# Obtiene la humedad y la temperatura desde el sensor 
				CO2=mq_CO2.MQPercentage()
				CO=mq_CO.MQPercentage()
				O3=mq_O3.MQPercentage()
				NH3=mq_NH3.MQPercentage()
				NO2=mq_NO2.MQNO2()		
				try:
					humedad, temperatura =read_sensor.get_data_temp_humidity() 
				except:
					humedad=65
					temperatura=28
					print("Problemas con el sensor de temperatura")

				print(temperatura)
				pm2,pm10=read_sensor.get_pm_data()
				
				if(pm2!='none' and pm10!='none'):
					data_manage.save_PM2(float('%.3f'% pm2))
					data_manage.save_PM10(float('%.3f'% pm10))
					
				data_manage.save_temperature(temperatura)
				data_manage.save_humidity(humedad)
				data_manage.save_CO2(float('%.3f'% CO2["PPM_RESPONSE"]))##Guarda el valor de CO2 con cuatro cifras significativas.
				data_manage.save_CO(float('%.3f'% CO["PPM_RESPONSE"]))##Guarda el valor de CO2 con cuatro cifras significativas.
				data_manage.save_O3(float('%.3f'% O3["PPM_RESPONSE"]))##Guarda el valor de CO2 con cuatro cifras significativas.
				data_manage.save_NH3(float('%.3f'% NH3["PPM_RESPONSE"]))##Guarda el valor de CO2 con cuatro cifras significativas.
				data_manage.save_NO2(float('%.3f'% NO2))##Guarda el valor de CO2 con cuatro cifras significativas.
				
				print("Funcionando")
				# Duerme 10 segundos
				time.sleep(2)
				
		# Se ejecuta en caso de que falle alguna instruccion dentro del try
			except Exception,e:
			# Imprime en pantalla el error e
				print str(e)	


if __name__=="__main__":
	main()