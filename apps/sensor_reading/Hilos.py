import time
import threading
import os

def startprgm(i):
    
    if (i == 0):
        os.system("sudo python /home/pi/Desktop/calidad_aire_no_user/apps/sensor_reading/main.py")
        print('Corriendo: Servidor')
    elif (i == 1):
        print('Corriendo datos: ')
        os.system("sudo python /home/pi/Desktop/calidad_aire_no_user/manage.py runserver 0.0.0.0:8000")
        time.sleep(1)
        
    else:
        pass

for i in range(2):
    t = threading.Thread(target=startprgm, args=(i,))
    time.sleep(0.5)
    t.start()   