
		var ctx = document.getElementById('grafica-humedad').getContext('2d');
		var grafica_humedad = new Chart(ctx, {
			    type: 'line',
		
			    
			    data: {
			        
			        datasets: [{
			            label: "Humedad",
			            backgroundColor: 'rgb(255, 99, 132)',
			            borderColor: 'rgb(255, 99, 132)',
			            data:[],
			            fill:false,

			        }]
			    },
			  options: {
	                scales: {
	                    yAxes: [{
	                            display: true,
	                            ticks: {
	                                beginAtZero: true,
	                                steps: 10,
	                                stepValue: 5,
	                                max: 100
	                            }
	                        }]
	                }
               } 
			});

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

		var ctx = document.getElementById('grafica-temperatura').getContext('2d');
		var grafica_temperatura = new Chart(ctx, {
    				
			    type: 'line',
			    
			    data: {
			        
			        datasets: [{
			            label: "Temperatura",
			            backgroundColor: 'rgb(255, 99, 132)',
			            borderColor: 'rgb(255, 99, 132)',
			            data:[],
			            fill:false,
			        }]
			    },

			    options: {
                scales: {
                    yAxes: [{
                            display: true,
                            ticks: {
                                beginAtZero: true,
                                steps: 20,
                                stepValue: 5,
                                max: 100,
                            }
                        }]
                }
               } 

			});

		
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
		var ctx = document.getElementById('grafica-co2').getContext('2d');
		var grafica_co2 = new Chart(ctx, {
    
			    type: 'line',
		
			    
			    data: {
			        
			        datasets: [{
			            label: "CO2",
			            backgroundColor: 'rgb(255, 99, 132)',
			            borderColor: 'rgb(255, 99, 132)',
			            data:[],
			            fill:false,
			        }]
			    },
			});

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
		var ctx = document.getElementById('grafica-co').getContext('2d');
		var grafica_co = new Chart(ctx, {
    
			    type: 'line',
		
			    
			    data: {
			        
			        datasets: [{
			            label: "CO",
			            backgroundColor: 'rgb(255, 99, 132)',
			            borderColor: 'rgb(255, 99, 132)',
			            data:[],
			            fill:false,
			        }]
			    },
			});

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

		var ctx = document.getElementById('grafica-o3').getContext('2d');
		var grafica_o3 = new Chart(ctx, {
    
			    type: 'line',
		
			    
			    data: {
			        
			        datasets: [{
			            label: "O3",
			            backgroundColor: 'rgb(255, 99, 132)',
			            borderColor: 'rgb(255, 99, 132)',
			            data:[],
			            fill:false,
			        }]
			    },
		
			});

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

		var ctx = document.getElementById('grafica-nh3').getContext('2d');
		var grafica_nh3 = new Chart(ctx, {
    
			    type: 'line',
		
			    
			    data: {
			        
			        datasets: [{
			            label: "NH3",
			            backgroundColor: 'rgb(255, 99, 132)',
			            borderColor: 'rgb(255, 99, 132)',
			            data:[],
			            fill:false,
			        }]
			    },
			});

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////


		var ctx = document.getElementById('grafica-no2').getContext('2d');
		var grafica_no2 = new Chart(ctx, {
    
			    type: 'line',
		
			    
			    data: {
			        
			        datasets: [{
			            label: "NO2",
			            backgroundColor: 'rgb(255, 99, 132)',
			            borderColor: 'rgb(255, 99, 132)',
			            data:[],
			            fill:false,
			        }]
			    },
			});
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////


		var ctx = document.getElementById('grafica-pm2').getContext('2d');
		var grafica_pm2 = new Chart(ctx, {
    
			    type: 'line',
		
			    
			    data: {
			        
			        datasets: [{
			            label: "PM2",
			            backgroundColor: 'rgb(255, 99, 132)',
			            borderColor: 'rgb(255, 99, 132)',
			            data:[],
			            fill:false,
			        }]
			    },
			});
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////


		var ctx = document.getElementById('grafica-pm10').getContext('2d');
		var grafica_pm10 = new Chart(ctx, {
    
			    type: 'line',
		
			    
			    data: {
			        
			        datasets: [{
			            label: "PM10",
			            backgroundColor: 'rgb(255, 99, 132)',
			            borderColor: 'rgb(255, 99, 132)',
			            data:[],
			            fill:false,
			        }]
			    },
			});
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////



var count=0
var jsonRequestUrl = '/data_request'  // URL Django data server 
console.log(jsonRequestUrl)
setInterval(()=>{

   $.ajax({ // Jquery Function to asyncronous HTTP request
  
        url: jsonRequestUrl,
        data: {
          'username': "username"
        },
        dataType: 'json',
        success: function (data) {
          document.getElementById("value_temp").innerHTML=data.Temp // Envia la informacion recibida el Front-end
          document.getElementById("value_hum").innerHTML=data.hum // Envia la informacion recibida el Front-end
          document.getElementById("value_co2").innerHTML=data.co2 // Envia la informacion recibida el Front-end
          document.getElementById("value_ozone").innerHTML=data.ozone //Envia la informacion recibida el Front-end
          document.getElementById("value_nh3").innerHTML=data.nh3 // Envia la informacion recibida el Front-end
          document.getElementById("value_co").innerHTML=data.co // Envia la informacion recibida el Front-end
          document.getElementById("value_no2").innerHTML=data.no2 // Envia la informacion recibida el Front-end
          document.getElementById("value_pm2").innerHTML=data.pm2 // Envia la informacion recibida el Front-end
          document.getElementById("value_pm10").innerHTML=data.pm10 // Envia la informacion recibida el Front-end

          push_data(grafica_humedad,data.hum)
          push_data(grafica_temperatura,data.Temp)
          push_data(grafica_co2,data.co2)
          push_data(grafica_o3,data.ozone)
          push_data(grafica_nh3,data.nh3)
          push_data(grafica_co,data.co)
          push_data(grafica_no2,data.no2)
          push_data(grafica_pm2,data.pm2)
          push_data(grafica_pm10,data.pm10)
         

          count+=1
          if(count==30)	{
          count=clear(grafica_humedad)
          count=clear(grafica_temperatura)
          count=clear(grafica_co2)
          count=clear(grafica_o3)
          count=clear(grafica_nh3)
          count=clear(grafica_co)
          count=clear(grafica_no2) 
      	  count=clear(grafica_pm2)
  		  count=clear(grafica_pm10)
  		}

          		
          }
      });
  },2000)  // Time interval for calling the request function 



function clear(chart){

          		chart.data.labels=[]
          		chart.data.datasets[0].data=[]
          		return(0)
          	}

function push_data(chart,dato){
		  chart.data.labels.push(".");
          chart.data.datasets[0].data.push(parseFloat(dato))
          chart.update()
}



$(document).ready(function () {

    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });

});