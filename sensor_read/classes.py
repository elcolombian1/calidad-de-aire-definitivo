import time 
import Adafruit_DHT 
import datetime 
import psycopg2
from PMS7003 import PMS7003	 as PMSENSOR


class save_data(): ## Save Data
	def __init__(self):
			try:
				self.db =psycopg2.connect(host="localhost",database="datos", user="pi", password="root")
				self.cursor=self.db.cursor()	
				print("Conexion Exitosa")
		
			except:
				print("Conexion a base de datos fallida")

	def save_temperature(self,value):
		self.cursor.execute("INSERT INTO sensor_reading_dato (tipo_dato,valor_dato,fecha) VALUES ('Temperatura',%s,%s)",(value,datetime.datetime.now()))
		self.db.commit()

	def save_humidity(self,value):	
		self.cursor.execute("INSERT INTO sensor_reading_dato (tipo_dato,valor_dato,fecha) VALUES ('Humedad',%s,%s)",(value,datetime.datetime.now()))
		self.db.commit()
	
	def save_CO2(self,value):
		self.cursor.execute("INSERT INTO sensor_reading_dato (tipo_dato,valor_dato,fecha) VALUES ('CO2',%s,%s)",(value,datetime.datetime.now()))
		self.db.commit()
	def save_O3(self,value):
		self.cursor.execute("INSERT INTO sensor_reading_dato (tipo_dato,valor_dato,fecha) VALUES ('O3',%s,%s)",(value,datetime.datetime.now()))
		self.db.commit()
	def save_CO(self,value):
		self.cursor.execute("INSERT INTO sensor_reading_dato (tipo_dato,valor_dato,fecha) VALUES ('CO',%s,%s)",(value,datetime.datetime.now()))
		self.db.commit()	
	def save_NH3(self,value):
		self.cursor.execute("INSERT INTO sensor_reading_dato (tipo_dato,valor_dato,fecha) VALUES ('NH3',%s,%s)",(value,datetime.datetime.now()))
		self.db.commit()	
	def save_NO2(self,value):
		self.cursor.execute("INSERT INTO sensor_reading_dato (tipo_dato,valor_dato,fecha) VALUES ('NO2',%s,%s)",(value,datetime.datetime.now()))
		self.db.commit()		
	def save_PM2(self,value):
		self.cursor.execute("INSERT INTO sensor_reading_dato (tipo_dato,valor_dato,fecha) VALUES ('PM2',%s,%s)",(value,datetime.datetime.now()))
	def save_PM10(self,value):
		self.cursor.execute("INSERT INTO sensor_reading_dato (tipo_dato,valor_dato,fecha) VALUES ('PM10',%s,%s)",(value,datetime.datetime.now()))
	def close_db(self):
		self.cursor.close()

class sensor_reader():
        def __init__(self):
		self.kind_sensor = Adafruit_DHT.DHT11 # Tipo de sensor de humedad usado
		self.temp_hum_pin = 18 ##Pin para recepcion de informacion desde pines del sensor de humedad

	def get_data_temp_humidity(self):##implemenatacion de metodo para obtener informacionde sensor de temperatura
                return(Adafruit_DHT.read_retry(self.kind_sensor,self.temp_hum_pin))

	def get_pm_data(self):
    		pm2_10=PMSENSOR()
	    	buffer_aux=pm2_10.cargar_buffer()
	    	pm2,pm10=pm2_10.return_data(buffer_aux)
	    	return(pm2,pm10)

